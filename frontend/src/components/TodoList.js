import React, { useState, useEffect } from "react";
import styles from "./../assets/scss/todo-list.module.scss";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button, Popconfirm, Spin } from "antd";
import { fetchTodos, removeTodos } from "../redux";
import { connect } from "react-redux";
import TodoModal from "./TodoFormModal";

const TodoList = ({ todoData, fetchTodos, removeTodos }) => {
  const [visibility, setVisibility] = useState(false);
  const [todoState, setTodo] = useState();

  useEffect(() => {
    async function fetchData() {
      fetchTodos();
    }
    fetchData();
  }, []);

  function deleteTodo(id) {
    removeTodos(id);
  }

  function openModal(todo) {
    setTodo(todo);
    setVisibility(true);
  }

  function closeModal() {
    setVisibility(false);
  }

  return (
    <div className={styles.todo_container}>
      {visibility && (
        <TodoModal
          todo={todoState}
          visible={visibility}
          closeModal={closeModal}
        />
      )}
      {todoData.isLoading && (
        <div className={styles.loading}>
          <Spin />
        </div>
      )}
      {todoData.todos.length === 0 && (
        <div className={styles.recentTodo}>
          <p>No Todo's added.</p>
        </div>
      )}
      <div className={styles.todo_section}>
        {todoData.todos.length > 0 && (
          <div className={styles.recentTodo}>
            <p>My Todo's.</p>
          </div>
        )}
        {todoData.todos.map((todo) => {
          return (
            <div key={todo.id} className={styles.single_item}>
              <div className={styles.todo_top}>
                <p className={styles.todo_title}>{todo.title}</p>
                <div>
                  <Button
                    type="primary"
                    className={styles.action_button}
                    onClick={() => {
                      openModal(todo);
                    }}
                  >
                    <EditOutlined />
                  </Button>
                  <Popconfirm
                    placement="topLeft"
                    title="Are you sure you want to delete?"
                    onConfirm={() => deleteTodo(todo.id)}
                  >
                    <Button type="danger">
                      <DeleteOutlined />
                    </Button>
                  </Popconfirm>
                </div>
              </div>
              <p className={styles.todo_description}>{todo.description}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

const makeMapStateToProps = (state) => {
  return {
    todoData: state.todos,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchTodos: () => dispatch(fetchTodos()),
    removeTodos: (id) => dispatch(removeTodos(id)),
  };
};

export default connect(makeMapStateToProps, mapDispatchToProps)(TodoList);
