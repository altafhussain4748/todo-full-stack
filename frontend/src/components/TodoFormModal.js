import React from "react";
import { Modal, Form, Input } from "antd";
import { updateTodos } from "../redux";
import { connect } from "react-redux";
const { TextArea } = Input;
const CollectionCreateForm = ({ visible, onCreate, onCancel, todo }) => {
  const [form] = Form.useForm();
  return (
    <Modal
      visible={visible}
      title="Update selected todo."
      okText="Update Todo"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            onCreate(values);
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          title: todo.title,
          description: todo.description,
        }}
      >
        <Form.Item
          name="title"
          label="Title"
          rules={[
            {
              required: true,
              message: "Please input the title of todo!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="description"
          label="Description"
          rules={[
            {
              required: true,
              message: "Please input the Description of todo!",
            },
          ]}
        >
          <TextArea rows={4} />
        </Form.Item>
      </Form>
    </Modal>
  );
};

const TodoModal = ({ visible, todo, closeModal, updateTodos }) => {
  const onCreate = (values) => {
    values.id = todo.id;
    updateTodos(values);
    closeModal();
  };

  return (
    <div>
      <CollectionCreateForm
        visible={visible}
        onCreate={onCreate}
        onCancel={() => {
          closeModal();
        }}
        todo={todo}
      />
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateTodos: (todo) => dispatch(updateTodos(todo)),
  };
};

export default connect(null, mapDispatchToProps)(TodoModal);
