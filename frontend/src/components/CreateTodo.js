import React, { useState } from "react";
import styles from "./../assets/scss/create-todo.module.scss";
import { TODO_URL } from "./../constants";
import { Spin, message } from "antd";
import { connect } from "react-redux";
import { addTodoToState } from "../redux";
const CreateTodo = ({ addTodoToState }) => {
  const [state, setForm] = useState({
    isLoading: false,
    error: false,
    title: "",
    description: "",
    msg: "",
  });

  function handleType(e) {
    setForm({ ...state, [e.target.name]: e.target.value });
  }

  function showMessage(msg) {
    message.success(msg);
  }

  async function onSubmit(e) {
    setForm({ ...state, isLoading: true });
    e.preventDefault();
    try {
      const res = await fetch(TODO_URL, {
        method: "POST",
        body: JSON.stringify({
          title: state.title,
          description: state.description,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      });
      const result = await res.json();
      addTodoToState(result);
      showMessage("Todo created successfully.");
      setForm({
        ...state,
        isLoading: false,
      });
    } catch (error) {
      setForm({
        isLoading: false,
      });
      showMessage("Something went wrong.");
    }
  }

  return (
    <div className={styles.form_container}>
      <form onSubmit={(e) => onSubmit(e)}>
        <p>Create New Todo</p>
        {state.isLoading && (
          <div className={styles.loading}>
            <Spin />
          </div>
        )}
        <div className={styles.form_group}>
          <label>Title:</label>
          <input
            type="text"
            name="title"
            className={styles.form_element}
            onChange={(e) => handleType(e)}
            value={state.title}
            required
          />
        </div>
        <div className={styles.form_group}>
          <label>Description:</label>
          <textarea
            name="description"
            id="description"
            cols="30"
            value={state.description}
            rows="3"
            className={styles.form_element}
            onChange={(e) => handleType(e)}
            required
          ></textarea>
        </div>
        <input type="submit" value="Add Todo" className={styles.btn} />
      </form>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    addTodoToState: (todo) => dispatch(addTodoToState(todo)),
  };
};

export default connect(null, mapDispatchToProps)(CreateTodo);
