import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import styles from "./assets/scss/app.module.scss";
import React from "react";
import CreateTodo from "./components/CreateTodo"
import TodoList from "./components/TodoList";
import { Provider } from "react-redux";
import store from "./redux/store";
import "antd/dist/antd.css";

function App() {
  return (
    <Provider store={store}>
      <div className={styles.App}>
        <CreateTodo />
        <Router>
          <Switch>
            <Route exact path="/" render={(props) => <TodoList {...props} />} />
          </Switch>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
