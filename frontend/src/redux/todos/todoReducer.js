import {
  FETCH_TODOS_REQUEST,
  FETCH_TODOS_SUCCESS,
  FETCH_TODOS_FAILURE,
  REMOVE_TODOS_SUCCESS,
  ADD_TODO_TO_STATE,
  UPDATE_TODO_TO_STATE,
} from "../types";

const initialState = {
  isLoading: false,
  todos: [],
  error: "",
};

const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TODOS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_TODOS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        todos: action.payload,
      };
    case FETCH_TODOS_FAILURE:
      return {
        ...state,
        isLoading: false,
        todos: [],
        error: "Error while fetching todo.",
      };
    case REMOVE_TODOS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        todos: state.todos.filter((item) => item.id !== action.payload),
        error: "Todo Removed.",
      };
    case ADD_TODO_TO_STATE:
      return {
        ...state,
        todos: [action.payload].concat(state.todos),
      };
    case UPDATE_TODO_TO_STATE:
      let todos = [...state.todos];
      var foundIndex = todos.findIndex((x) => x.id === action.payload.id);
      todos[foundIndex] = action.payload;
      return {
        ...state,
        todos
      };
    default:
      return state;
  }
};

export default todoReducer;
