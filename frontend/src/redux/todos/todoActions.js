import { TODO_URL } from "../../constants";
import {
  FETCH_TODOS_REQUEST,
  FETCH_TODOS_SUCCESS,
  FETCH_TODOS_FAILURE,
  REMOVE_TODOS_SUCCESS,
  ADD_TODO_TO_STATE,
  UPDATE_TODO_TO_STATE,
} from "../types";

export const fetchTodos = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchTodosRequest());
      const response = await fetch(TODO_URL);
      const todos = await response.json();
      dispatch(fetchTodosSuccess(todos));
    } catch (err) {
      dispatch(fetchTodosFailure(err.message));
    }
  };
};

export const removeTodos = (id) => {
  return async (dispatch) => {
    try {
      await fetch(`${TODO_URL}${id}/`, {
        method: "DELETE",
      });
      dispatch(removeTodosSuccess(id));
    } catch (err) {
      console.log(err);
      dispatch(fetchTodosFailure(err.message));
    }
  };
};

export const updateTodos = (todo) => {
  return async (dispatch) => {
    try {
      await fetch(`${TODO_URL}${todo.id}/`, {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(todo),
      });
      dispatch(updateTodoToState(todo));
    } catch (err) {
      dispatch(fetchTodosFailure(err.message));
    }
  };
};

export const addTodoToState = (todo) => {
  return (dispatch) => {
    dispatch({
      type: ADD_TODO_TO_STATE,
      payload: todo,
    });
  };
};

export const updateTodoToState = (todo) => {
  return {
    type: UPDATE_TODO_TO_STATE,
    payload: todo,
  };
};

export const fetchTodosRequest = () => {
  return {
    type: FETCH_TODOS_REQUEST,
  };
};

export const fetchTodosSuccess = (todos) => {
  return {
    type: FETCH_TODOS_SUCCESS,
    payload: todos,
  };
};

export const fetchTodosFailure = (error) => {
  return {
    type: FETCH_TODOS_FAILURE,
    payload: error,
  };
};

export const removeTodosSuccess = (id) => {
  console.log(id);
  return {
    type: REMOVE_TODOS_SUCCESS,
    payload: id,
  };
};
