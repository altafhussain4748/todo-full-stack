from . import views
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register("todo", views.TodoViewSet, "todo")

urlpatterns = router.urls